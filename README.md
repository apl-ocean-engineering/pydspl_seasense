# libdspl_seasense

Python library which implements the
[Deep Sea Power and Light](https://www.deepsea.com/)
SeaSense protocol for controlling their lights, cameras, etc.

At present we only have a pair of [LSL-2000 SeaLites](https://www.deepsea.com/portfolio-items/led-sealite/) for testing, so only the functionalities for daily
operation of the lights is implemented and tested.
In particular, very few of the one-off configuration
commands (change baud rate, set user presets, etc.) are implemented by
the module.

The library has been written against DSPL-provided
[protocol document (PDF)](https://www.deepsea.com/wp-content/uploads/SeaSense_Protocol_Specification.pdf).

The library implements:

 * A barebones functional interface for creating Seasense message.
 * Class wrappers for a generic peripheral, and a Sealite LED lamp.

## Minimum example

```
import serial
from pydspl_seasense import sealite

with serial.serial_for_url("/dev/ttyUSB0") as fp:

  sealite = sealite.Sealite(address="001")

  print(sealite.read_temperature(fp))
```

# Seasense Protocol Cheatsheet

Units default to 9600 baud but can be set to 19200 or 57600.

## Common commands handled by pydspl_seasense

All sample commands target node 1, and the CR-LF line ending is not shown

* __Query node address__:   `!001:ADDR?`
* __Query unit temperature__: `!001:TEMP?`
* __Query unit humidity__: `!001:RHUM?`
* __Get unit info__ (serial number, firmware, etc.): `!001:INFO?`
* __Get unit status__: `!001:STAT?`

* __Set light level (x = 0-100)__: `!001:LOUT=x`
* __Query light level__: `!001:LOUT?`

## Common commands not handled by pydspl_seasense

#### Change node address

Change node 001 to be node 002:

```
!001:ADDR=002<cr><lf>
```


# License

[BSD](LICENSE.txt)
