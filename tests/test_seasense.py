import pytest
import unittest

from pydspl_seasense.seasense import build_packet


class SeasenseBuildPacket(unittest.TestCase):
    def test_address_not_integer(self):
        with pytest.raises(ValueError):
            build_packet(None, "INFO", "?")

        with pytest.raises(ValueError):
            build_packet("a", "INFO", "?")

    def test_address_too_low(self):
        with pytest.raises(ValueError):
            build_packet(-1, "INFO", "?")

    def test_address_too_high(self):
        with pytest.raises(ValueError):
            build_packet(1024, "INFO", "?")

    def test_command_not_string(self):
        with pytest.raises(ValueError):
            build_packet(1, 1, "?")

        with pytest.raises(ValueError):
            build_packet(1, "c", "?")

    def test_command_wrong_length(self):
        with pytest.raises(ValueError):
            build_packet(999, "INF", "?")

        with pytest.raises(ValueError):
            build_packet(999, "LIGHT", "?")

    def test_command_bad_command(self):
        with pytest.raises(ValueError):
            build_packet(999, "INFO", "$")

    def test_command_valid(self):
        self.assertEqual(build_packet(999, "INFO", "?"), "!999:INFO?*9b\r\n")

        self.assertEqual(build_packet(999, "INFO", "?"), "!999:INFO?*9b\r\n")

        self.assertEqual(build_packet("999", "INFO", "?"), "!999:INFO?*9b\r\n")

        # This is an example given in the DSPL documentation
        self.assertEqual(build_packet("010", "lout", "?"), "!010:lout?*19\r\n")


if __name__ == "__main__":
    unittest.main()
