import unittest
from unittest.mock import Mock

from pydspl_seasense.peripheral_base import PeripheralBase


class TestPeripheralBase(unittest.TestCase):
    def test_construction(self):
        peripheral = PeripheralBase(100)

        self.assertEqual(peripheral.address, 100)

    def test_read_info(self):
        # Example from DSPL documentation
        port = Mock()
        port.read_until = Mock(
            return_value=b"010,SLS-5100,45-00101,0A,1.0.1,W60,FLD,A05"
        )

        peripheral = PeripheralBase(10)
        result = peripheral.read_info(port)

        self.assertEqual(result, "010,SLS-5100,45-00101,0A,1.0.1,W60,FLD,A05")

    def test_read_temperature(self):
        # Example from DSPL documentation
        port = Mock()
        port.read_until = Mock(return_value=b"27.25")

        peripheral = PeripheralBase(address="010")
        result = peripheral.read_temperature(port)

        self.assertEqual(result, 27.25)

    def test_read_humidity(self):
        # Example from DSPL documentation
        port = Mock()
        port.read_until = Mock(return_value=b"65535")

        peripheral = PeripheralBase(address="010")
        result = peripheral.read_humidity(port)

        self.assertEqual(result, 65535)
