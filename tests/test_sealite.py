import unittest
from unittest.mock import Mock

from pydspl_seasense.sealite import Sealite


class TestSealight(unittest.TestCase):
    def test_construction_no_initialize(self):
        port = Mock()
        port.read_until = Mock(return_value=b"0")

        sealite = Sealite(100)

        self.assertEqual(sealite.address, 100)
        self.assertEqual(sealite.level, 0)

    def test_construction_with_initialize(self):
        port = Mock()
        port.read_until = Mock(return_value=b"0")

        sealite = Sealite(100, fp=port)

        self.assertEqual(sealite.address, 100)
        self.assertEqual(sealite.level, 0)

    def test_set_level(self):
        port = Mock()
        port.write = Mock()
        port.read_until = Mock(return_value=b"0")

        sealite = Sealite(100, fp=port)
        port.write.assert_called_with(b"!100:lout?*19\r\n")

        port.read_until = Mock(return_value=b"\r\n")
        sealite.set_level(fp=port, level=100)

        port.write.assert_called_with(b"!100:lout=100*a8\r\n")

    def test_increment(self):
        port = Mock()
        port.write = Mock()

        # Tell module that light is at level 80
        port.read_until = Mock(return_value=b"80")

        sealite = Sealite(100, fp=port)
        port.write.assert_called_with(b"!100:lout?*19\r\n")

        # Mock will return ACK
        port.read_until = Mock(return_value=b"\r\n")

        self.assertEqual(sealite.increment_level(fp=port), True)
        port.write.assert_called_with(b"!100:lout+10*66\r\n")
        self.assertEqual(sealite._level, 90)

        self.assertEqual(sealite.increment_level(fp=port), True)
        port.write.assert_called_with(b"!100:lout+10*66\r\n")
        self.assertEqual(sealite._level, 100)

        # Should fail, already at 100
        self.assertEqual(sealite.increment_level(fp=port), False)

        # Force it to send, even though the level should be beyond max
        self.assertEqual(sealite.increment_level(fp=port, force=True), True)
        port.write.assert_called_with(b"!100:lout+10*66\r\n")
        self.assertEqual(sealite._level, 100)

    def test_decrement(self):
        port = Mock()
        port.write = Mock()

        # "Light" mock should return level of 20
        port.read_until = Mock(return_value=b"20")

        sealite = Sealite(100, fp=port)
        port.write.assert_called_with(b"!100:lout?*19\r\n")

        # Mock will return ACK
        port.read_until = Mock(return_value=b"\r\n")

        self.assertEqual(sealite.decrement_level(fp=port), True)
        port.write.assert_called_with(b"!100:lout-10*68\r\n")
        self.assertEqual(sealite._level, 10)

        self.assertEqual(sealite.decrement_level(fp=port), True)
        port.write.assert_called_with(b"!100:lout-10*68\r\n")
        self.assertEqual(sealite._level, 0)

        # Should fail, already at 0
        self.assertEqual(sealite.decrement_level(fp=port), True)
        port.write.assert_called_with(b"!100:lout=0*47\r\n")
        self.assertEqual(sealite._level, 0)
